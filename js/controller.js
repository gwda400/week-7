
app.controller("homeController", function ($scope) {
    $scope.page = content[0];
    $scope.myClass = 'homepage';

    $scope.buttons = [
        { text: "Dog", active: true },
        { text: "Cat", active: true },
        { text: "Goose", active: true },
    ];
});

app.controller("contactController", function ($scope) {
    $scope.page = content[2];
    $scope.myClass = 'contactpage';
})

app.controller("aboutController", function ($scope) {
    $scope.page = content[1];
});

app.directive('myDirective', function () {
    return {
        restrict: 'E',
        templateUrl: 'sample.html'
    }

});

var content = [
	{
	    title: "Angular Example",
	    description: 'This example is a quick exercise to illustrate how dynamic content can be added to a page. To start the process, lets hit the navbar above',
	    active: true
	},
	{
	    title: "About",
	    description: 'The about description',
	    active: false
	},
	{
	    title: "Contact",
	    description: 'Contact Information',
	    active: false
	},
];